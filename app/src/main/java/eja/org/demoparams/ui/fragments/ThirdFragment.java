package eja.org.demoparams.ui.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;

import eja.org.demoparams.R;
import roboguice.inject.InjectView;

/**
 * Created by cocoy on 06/02/2016.
 */
public class ThirdFragment extends BaseFragment {

    @InjectView(R.id.boton) private Button boton;
    @InjectView(R.id.etiqueta) private TextView etiqueta;
    @InjectView(R.id.param) private EditText param;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.third_fragment_layout,container,false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if(preferencesHelper.isViajero())
            etiqueta.setText("Traia un viajero");

        boton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                preferencesHelper.saveViajero(false);
                Intent intento = new Intent();
                intento.putExtra("parametro",param.getText().toString());
                getActivity().setResult(Activity.RESULT_OK,intento);
                getActivity().finish();
            }
        });

    }

}
