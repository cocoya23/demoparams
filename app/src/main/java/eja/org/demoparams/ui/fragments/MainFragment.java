package eja.org.demoparams.ui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.Toast;

import eja.org.demoparams.R;
import eja.org.demoparams.ui.activities.SecondActivity;
import eja.org.demoparams.ui.activities.ThirdActivity;
import roboguice.inject.InjectView;

/**
 * Created by cocoy on 06/02/2016.
 */
public class MainFragment extends BaseFragment {

    @InjectView(R.id.primerBoton) private Button primerBoton;
    @InjectView(R.id.segundoBoton) private Button segundoBoton;
    @InjectView(R.id.primerParam) private EditText primerParam;
    @InjectView(R.id.viajero) private Switch viajero;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.main_fragment_layout,container,false);

    }

    @Override
    public void onResume() {
        super.onResume();
        if(preferencesHelper.isViajero()){
            viajero.setChecked(true);
            viajero.setVisibility(View.VISIBLE);
        } else {
            viajero.setChecked(false);
            viajero.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        primerBoton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lanzaSegundaActividad();
            }
        });

        segundoBoton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lanzaTerceraActividad();
            }
        });

    }

    private void lanzaTerceraActividad() {
        Intent intento = new Intent(getActivity(),ThirdActivity.class);
        startActivityForResult(intento,0);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 0 && resultCode == getActivity().RESULT_OK){
            Toast.makeText(getActivity(),data.getStringExtra("parametro"),Toast.LENGTH_LONG).show();
        }
    }

    private void lanzaSegundaActividad() {

        String parametro = primerParam.getText().toString();
        Toast.makeText(getActivity(),parametro,Toast.LENGTH_LONG);
        Intent intento = new Intent(getActivity(),SecondActivity.class);
        intento.putExtra("parametro",parametro);
        startActivity(intento);
        
    }
}
