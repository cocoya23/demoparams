package eja.org.demoparams.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Switch;
import android.widget.TextView;

import eja.org.demoparams.R;
import roboguice.inject.InjectView;

/**
 * Created by cocoy on 06/02/2016.
 */
public class SecondFragment extends BaseFragment {

    @InjectView(R.id.boton) private Button boton;
    @InjectView(R.id.etiqueta) private TextView etiqueta;
    @InjectView(R.id.viajero) private Switch viajero;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.second_fragment_layout,container,false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        etiqueta.setText(getArguments().getString("parametro"));

        boton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(viajero.isChecked())preferencesHelper.saveViajero(true);
                else preferencesHelper.saveViajero(false);
                getActivity().finish();
            }
        });

    }
}
