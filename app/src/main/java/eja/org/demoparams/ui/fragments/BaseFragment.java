package eja.org.demoparams.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.inject.Inject;

import eja.org.demoparams.R;
import eja.org.demoparams.core.commons.helpers.PreferencesHelper;
import roboguice.fragment.RoboFragment;

/**
 * Created by cocoy on 06/02/2016.
 */
public class BaseFragment extends RoboFragment {

    @Inject
    protected PreferencesHelper preferencesHelper;

}
