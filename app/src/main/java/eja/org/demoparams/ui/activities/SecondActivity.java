package eja.org.demoparams.ui.activities;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.widget.FrameLayout;

import eja.org.demoparams.R;
import eja.org.demoparams.ui.fragments.MainFragment;
import eja.org.demoparams.ui.fragments.SecondFragment;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;

/**
 * Created by cocoy on 06/02/2016.
 */

@ContentView(R.layout.frame_activity_layout)
public class SecondActivity extends BaseFragmentActivity {

    @InjectView(R.id.rootFrame)
    private FrameLayout rootFrame;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FragmentManager fragmentManager = getSupportFragmentManager();

        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        Bundle bundle = new Bundle();
        bundle.putString("parametro", getIntent().getStringExtra("parametro"));

        SecondFragment secondFragment = new SecondFragment();
        secondFragment.setArguments(bundle);
        fragmentTransaction.add(R.id.rootFrame,secondFragment);

        fragmentTransaction.commit();

    }

}
