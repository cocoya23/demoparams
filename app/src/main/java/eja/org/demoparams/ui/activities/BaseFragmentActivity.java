package eja.org.demoparams.ui.activities;

import android.os.Bundle;

import com.google.inject.Inject;

import eja.org.demoparams.core.commons.helpers.PreferencesHelper;
import roboguice.activity.RoboFragmentActivity;

/**
 * Created by cocoy on 06/02/2016.
 */
public class BaseFragmentActivity extends RoboFragmentActivity {

    @Inject
    protected PreferencesHelper preferencesHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
}
