package eja.org.demoparams.core.commons.helpers;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.inject.Inject;

import roboguice.inject.ContextSingleton;

@ContextSingleton
public class PreferencesHelper {

    private final String PREFERENCES_TAG = "DemoParams";
    private final String VIAJERO_PREFERENCE = "VIAJERO";
    private SharedPreferences preferences;

    @Inject
    public PreferencesHelper(Context context){
        preferences = context.getSharedPreferences(PREFERENCES_TAG,Context.MODE_PRIVATE);
    }

    public void saveViajero(boolean viajero){
        preferences.edit().putBoolean(VIAJERO_PREFERENCE,viajero).commit();
    }

    public boolean isViajero(){
        return preferences.getBoolean(VIAJERO_PREFERENCE,false);
    }

}
